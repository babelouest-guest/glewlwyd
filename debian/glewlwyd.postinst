#!/bin/sh
set -e

# Source debconf library.
. /usr/share/debconf/confmodule

# Code copied from dh_systemd_enable ----------------------
# This will only remove masks created by d-s-h on package removal.
deb-systemd-helper unmask glewlwyd.service >/dev/null || true

if deb-systemd-helper --quiet was-enabled glewlwyd.service; then
  # Enables the unit on first installation, creates new
  # symlinks on upgrades if the unit file has changed.
  deb-systemd-helper enable glewlwyd.service >/dev/null || true
else
  # Update the statefile to add new symlinks (if any), which need to be
  # cleaned up on purge. Also remove old symlinks.
  deb-systemd-helper update-state glewlwyd.service >/dev/null || true
fi

# Create the glewlwyd user in glewlwyd, so that Dbus doesn't complain.
      
# create a user to run as (code stolen from dnsmasq)
if [ "$1" = "configure" ]; then
  CONFIG_DIR="/etc/glewlwyd"
  CONFIG="$CONFIG_DIR/glewlwyd.conf"
  CONFIG_DB="$CONFIG_DIR/glewlwyd-db.conf"
  
  db_get glewlwyd/config_type || true
  
  if [ "$RET" = "Personalized" ]; then
    mkdir -p $CONFIG_DIR || true
    cp /usr/share/glewlwyd/templates/glewlwyd-debian.conf.properties $CONFIG
    
    if [ -z "`id -u glewlwyd 2> /dev/null`" ]; then
      echo "Add user glewlwyd"
      adduser --system  --home /etc/glewlwyd --gecos "glewlwyd" \
              --no-create-home --disabled-password \
              --quiet glewlwyd || true
    fi
    
    if [ ! -f /var/log/glewlwyd.log ]; then
      touch /var/log/glewlwyd.log
      chown glewlwyd /var/log/glewlwyd.log
    fi
    
    dbc_generate_include="template:$CONFIG_DB"
    dbc_generate_include_perms="660"
    dbc_generate_include_owner="glewlwyd:root"
    dbc_generate_include_args="-o template_infile=/usr/share/glewlwyd/templates/glewlwyd-db.conf.properties"
    dbc_mysql_createdb_encoding="UTF8"
    dbc_basepath="/var/cache/glewlwyd"
    dbc_dbfile_owner="glewlwyd:root"
    dbc_dbfile_perms="0660"

    . /usr/share/dbconfig-common/dpkg/postinst
    dbc_go glewlwyd "$@" || true
    
    db_get glewlwyd/config_external_url || true
    sed -i -e "s,_G_EXTRNAL_URL_/,$RET,g" $CONFIG
    
    if [ "$dbc_dbtype" = "mysql" ]; then
      sed -i -e "s,_G_DB_TYPE_,mariadb," $CONFIG_DB
      gunzip --stdout /usr/share/doc/glewlwyd/init.mariadb.sql.gz | mysql -u$dbc_dbuser -p$dbc_dbpass -h$dbc_dbserver $dbc_dbname
    elif [ "$dbc_dbtype" = "sqlite3" ]; then
      echo "Initializing SQLite3 database"
      sed -i -e "s,_G_DB_TYPE_,sqlite3," $CONFIG_DB
      su - glewlwyd -s /bin/sh -c "gunzip --stdout /usr/share/doc/glewlwyd/init.sqlite3.sql.gz | sqlite3 $dbc_basepath/$dbc_dbname"
    else
      sed -i -e "s,_G_DB_TYPE_,pgsql," $CONFIG_DB
      gunzip --stdout /usr/share/doc/glewlwyd/init.postgre.sql.gz | psql -u$dbc_dbuser -p$dbc_dbpass -h$dbc_dbserver $dbc_dbname
    fi
    
  else
    if [ ! -f $CONFIG ]; then
      gunzip --stdout /usr/share/doc/glewlwyd/glewlwyd.conf.sample.gz > $CONFIG
    fi
  fi
  
  echo "Start Glewlwyd service"
  invoke-rc.d glewlwyd start || true
fi
#DEBHELPER#
