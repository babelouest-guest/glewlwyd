Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: glewlwyd
Source: https://github.com/babelouest/glewlwyd

Files: *
Copyright: 2017-2019 Nicolas Mora <mail@babelouest.org>
License: GPL-3

Files: CMakeLists.txt
Copyright: 2018 Silvio Clecio <silvioprog@gmail.com>
           2018 Nicolas Mora <mail@babelouest.org>
License: Expat

Files: cmake-modules/*
Copyright: 2018 Silvio Clecio <silvioprog@gmail.com>
           2018 Nicolas Mora <mail@babelouest.org>
License: Expat

Files: cmake-modules/DownloadProject.*
Copyright: 2018 Craig Scott <contact@crascit.com>
           2018 Henry Schreiner <henry.fredrick.schreiner@cern.ch>
           2018 Chrisstaite
License: Expat

Files: */browser*js
Copyright: 2009 Thomas Robinson <280north.com>
           2011 Joyent, Inc. and other Node contributors.
           2013 Yusuke Suzuki <utatane.tea@gmail.com>
           2013-2014 Yusuke Suzuki <utatane.tea@gmail.com>
           2014 Ivan Nikulin <ifaaan@gmail.com>
           2014, 2015 Simon Lydell
           2014, Facebook, Inc.
           2013-2014 Olov Lassus <olov.lassus@gmail.com>
           2014 Benjamin Tan <https://d10.github.io/>
           2009-2011 Mozilla Foundation and contributors
           2011 The Closure Compiler Authors. All rights reserved.
           2011-2015, The Dojo Foundation All Rights Reserved.
License: Expat or BSD-2 or BSD-3

Files: */jquery*js
Copyright: 2016 jQuery Foundation and other contributors
License: Expat

Files: */react-bootstrap*js*
Copyright: 2016 Jed Watson.
           2013-2017, Facebook, Inc.
License: expat or BSD-3

Files: */react*js
Copyright: 2013-2017, Facebook, Inc.
License: BSD-3

Files: */react-dom*js
Copyright: 2013-2017, Facebook, Inc.
           2012-2013 TJ Holowaychuk
           2015 Andreas Lubbe
           2015 Tiancheng "Timothy" Gu
License: Expat or BSD-3

Files: */js.cookie*js
Copyright: 2006, 2015 Klaus Hartl & Fagner Brack
License: Expat

Files: */bootstrap*js
Copyright: 2011-2016 Twitter, Inc.
License: Expat

Files: */bootstrap*css
Copyright: 2011-2016 Twitter, Inc.
License: Expat

Files: */bootstrap-theme*css
Copyright: 2011-2016 Twitter, Inc.
License: Expat

Files: */bootstrap.vertical-tabs*css
Copyright: 2016 İsmail Demirbilek
License: Expat

Files: */font-awesome*css
Copyright: 2016 Dave Gandy
License: Expat

Files: test/*.c
Copyright: 2019 nobody
License: public-domain

Files: webapp/fonts/*
Copyright: 2016 Dave Gandy
License: SIL-OFL-1.1
License-Comment: obtained from http://fontawesome.io/license/

Files: debian/*
Copyright: 2017 Thorsten Alteholz <debian@alteholz.de>
           2019 Nicolas Mora <nicolas@babelouest.org>
License: GPL-3

License: GPL-3
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 3 of the License
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: public-domain
 Public domain, no copyright. Use at your own risk.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the 'Software'), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

License: BSD-2
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
   * Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 'AS IS'
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3
 Redistribution and use in source and binary forms, with or without 
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the 
    documentation and/or other materials provided with the distribution.
 .
 3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived 
    from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE 
 COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 POSSIBILITY OF SUCH DAMAGE.

License: SIL-OFL-1.1
 -----------------------------------------------------------
 SIL OPEN FONT LICENSE Version 1.1 - 26 February 2007
 -----------------------------------------------------------
 .
 PREAMBLE
 The goals of the Open Font License (OFL) are to stimulate worldwide
 development of collaborative font projects, to support the font creation
 efforts of academic and linguistic communities, and to provide a free and
 open framework in which fonts may be shared and improved in partnership
 with others.
 .
 The OFL allows the licensed fonts to be used, studied, modified and
 redistributed freely as long as they are not sold by themselves. The
 fonts, including any derivative works, can be bundled, embedded, 
 redistributed and/or sold with any software provided that any reserved
 names are not used by derivative works. The fonts and derivatives,
 however, cannot be released under any other type of license. The
 requirement for fonts to remain under this license does not apply
 to any document created using the fonts or their derivatives.
 .
 DEFINITIONS
 "Font Software" refers to the set of files released by the Copyright
 Holder(s) under this license and clearly marked as such. This may
 include source files, build scripts and documentation.
 .
 "Reserved Font Name" refers to any names specified as such after the
 copyright statement(s).
 .
 "Original Version" refers to the collection of Font Software components as
 distributed by the Copyright Holder(s).
 .
 "Modified Version" refers to any derivative made by adding to, deleting,
 or substituting -- in part or in whole -- any of the components of the
 Original Version, by changing formats or by porting the Font Software to a
 new environment.
 .
 "Author" refers to any designer, engineer, programmer, technical
 writer or other person who contributed to the Font Software.
 .
 PERMISSION & CONDITIONS
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of the Font Software, to use, study, copy, merge, embed, modify,
 redistribute, and sell modified and unmodified copies of the Font
 Software, subject to the following conditions:
 .
 1) Neither the Font Software nor any of its individual components,
 in Original or Modified Versions, may be sold by itself.
 .
 2) Original or Modified Versions of the Font Software may be bundled,
 redistributed and/or sold with any software, provided that each copy
 contains the above copyright notice and this license. These can be
 included either as stand-alone text files, human-readable headers or
 in the appropriate machine-readable metadata fields within text or
 binary files as long as those fields can be easily viewed by the user.
 .
 3) No Modified Version of the Font Software may use the Reserved Font
 Name(s) unless explicit written permission is granted by the corresponding
 Copyright Holder. This restriction only applies to the primary font name as
 presented to the users.
 .
 4) The name(s) of the Copyright Holder(s) or the Author(s) of the Font
 Software shall not be used to promote, endorse or advertise any
 Modified Version, except to acknowledge the contribution(s) of the
 Copyright Holder(s) and the Author(s) or with their explicit written
 permission.
 .
 5) The Font Software, modified or unmodified, in part or in whole,
 must be distributed entirely under this license, and must not be
 distributed under any other license. The requirement for fonts to
 remain under this license does not apply to any document created
 using the Font Software.
 .
 TERMINATION
 This license becomes null and void if any of the above conditions are
 not met.
 .
 DISCLAIMER
 THE FONT SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT
 OF COPYRIGHT, PATENT, TRADEMARK, OR OTHER RIGHT. IN NO EVENT SHALL THE
 COPYRIGHT HOLDER BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 INCLUDING ANY GENERAL, SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL
 DAMAGES, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF THE USE OR INABILITY TO USE THE FONT SOFTWARE OR FROM
 OTHER DEALINGS IN THE FONT SOFTWARE.
